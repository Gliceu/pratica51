package utfpr.ct.dainf.if62c.pratica;
//@author loja

public class SomaMatrizesIncompativeisException extends MatrizesIncompativeisException {

    public SomaMatrizesIncompativeisException(Matriz m1, Matriz m2) {
        super(m1, m2);
    }

    public Matriz soma(Matriz m1, Matriz m2) throws MatrizInvalidaException {
        Matriz s = new Matriz(m1.getMatriz().length, m1.getMatriz()[0].length);
        if (m1.getMatriz().length != m2.getMatriz().length || m1.getMatriz()[0].length != m2.getMatriz()[0].length) {
            throw new SomaMatrizesIncompativeisException(m1, m2);
        } else {
            for (int i = 0; i < m1.getMatriz().length; i++) {
                for (int j = 0; j < m1.getMatriz()[i].length; j++) {
                    s.getMatriz()[i][j] = m1.getMatriz()[i][j] + m2.getMatriz()[i][j];
                }
            }
        }
        return s;
    }
}
