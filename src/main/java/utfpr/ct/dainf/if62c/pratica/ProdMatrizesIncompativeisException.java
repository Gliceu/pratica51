package utfpr.ct.dainf.if62c.pratica;

public class ProdMatrizesIncompativeisException extends MatrizesIncompativeisException {

    public ProdMatrizesIncompativeisException(Matriz m1, Matriz m2) {
        super(m1, m2);
    }

    public Matriz prodMat(Matriz m1, Matriz m2) throws MatrizInvalidaException {

        Matriz p = new Matriz(m1.getMatriz().length, m1.getMatriz()[0].length);
        if (m1.getMatriz()[0].length != m2.getMatriz().length) {
            throw new ProdMatrizesIncompativeisException(super.getM1(), super.getM2());

        } else {
            for (int i = 0; i < m1.getMatriz().length; i++) {
                for (int j = 0; j < m2.getMatriz()[i].length; j++) {
                    for (int k = 0; k < m2.getMatriz().length; k++) {
                        p.getMatriz()[i][j] += m1.getMatriz()[i][k] * m2.getMatriz()[k][j];
                    }
                }
            }
        }
        return p;
    }
}
